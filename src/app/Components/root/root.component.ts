import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { takeUntil, debounceTime, switchMap, distinctUntilChanged, catchError, filter, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Subject, EMPTY } from 'rxjs';

import { Search } from '../../Model/Search.interface';
import { SearchService } from './../../search.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit, OnDestroy {
  private unsubscribe: Subject<void> = new Subject<void>();
  public isError = false;
  public errorText: string;
  public resultSearch: Search[] = [];
  public resultError: string;
  public firstSearch = false;
  public myListFilms: Search[] = [];
  public filmDetails: Search;

  title = 'films';
  search = new FormControl('');

  constructor(
    private searchService: SearchService,
    private router: Router
  ) { }

  ngOnInit() {
    this.search.valueChanges.pipe(
      map(text => {
        this.resultError = '';
        return text;
      }),
      filter(text => text.length > 0),
      debounceTime(450),
      distinctUntilChanged(),
      switchMap(value => {
        return this.searchService.searchFilm(value, 's').pipe(
          catchError(error => {
            this.isError = true;
            this.errorText = error.error.Error;

            return EMPTY;
          })
        );
      }),
      takeUntil(this.unsubscribe)
    )
      .subscribe(response => {
        if (response.Response === 'True') {
          this.resultSearch = response.Search;
          this.resultError = '';
        } else {
          this.resultSearch = [];
          this.resultError = response.Error;
        }
        this.isError = false;
      });
  }

  detailsFilm(id: string) {
    this.router.navigate(['/film', id]);
  }

  addList(film: Search) {
    this.myListFilms = [...this.myListFilms, film];
  }

  delFilm(delFilmId: string) {
    this.myListFilms = this.myListFilms.filter(film => film.imdbID !== delFilmId);

  }

  choiceFilm(film: Search) {
    this.filmDetails = film;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
