import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pipe, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';

import { SearchService } from './../../search.service';
import { DataFilm } from './../../Model/DataFilm.interface';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit, OnDestroy {
  public film: DataFilm;
  private unsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private activateRoute: ActivatedRoute,
    private searchService: SearchService) {
  }

  ngOnInit() {
    this.activateRoute.params.pipe(
      switchMap(({ id }) => {
        return this.searchService.searchFilm(id, 'i');
      }),
      takeUntil(this.unsubscribe)
    )

      .subscribe(data => {
        console.log('data', data)
        this.film = data;
      });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
