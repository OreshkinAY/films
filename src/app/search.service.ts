import { Injectable } from '@angular/core';
import { HttpApiService } from './http-api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(private httpApi: HttpApiService) { }

    public searchFilm(nameFilm, key): Observable<any> {
        return this.httpApi.get(nameFilm, key);
    }
}
