import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpApiService {
  private URL = 'http://www.omdbapi.com/?';
  private apikey = 'b8696789';

  constructor(private http: HttpClient) { }

  public get(nameFilm: string, key: string): Observable<any> {
    return this.http.get(`${this.URL + key}=${nameFilm}&apikey=${this.apikey}`);
  }
}
