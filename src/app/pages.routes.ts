import { Routes } from '@angular/router';
import { FilmComponent } from './Components/film/film.component';
import { RootComponent } from './Components/root/root.component';

export const PagesRoutes: Routes = [
  {
    path: 'film/:id',
    component: FilmComponent
  },
  {
    path: '',
    component: RootComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/'
  }
];
