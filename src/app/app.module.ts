import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpApiService } from './http-api.service';
import { HttpClientModule } from '@angular/common/http';

import {MaterialModule} from './material-module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FilmComponent } from './Components/film/film.component';
import { PagesRoutes } from './pages.routes';
import { RootComponent } from './Components/root/root.component';
@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    RootComponent
  ],
  imports: [
    RouterModule.forChild(PagesRoutes),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [HttpApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
